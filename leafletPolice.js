/* global L, iconB64Src, loaderB64Src */


String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

var markers;

var visited =[];
var markers = L.markerClusterGroup();


function foog(arr) {
    var a = [], b = [], prev;

    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if ( arr[i] !== prev ) {
            a.push(arr[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i];
    }

    return [a, b];
}

L.policeLayer = L.LayerGroup.extend({
    initialize: function (options) {
        var prefKeys = [["displayThumbnail", true], ["displayPosition", true], ["displayTypes", true],
                        ["displayAbstract", true], ["displayLink", true], ["includeCities", false],
                        ["displayMarkerLabel", true], ["lang", "en"]];
        this.dbp.prefs = {};
        for (var key in prefKeys) {
            this.dbp.prefs[prefKeys[key][0]] = (
                options[prefKeys[key][0]] === undefined ?
                prefKeys[key][1] :
                options[prefKeys[key][0]]);
        }
        this.dbp.prefs.loaderGif = options.loaderGif || null;
        this.dbp.prefs.icon = options.icon || null;
        this.dbp.icon = this.dbp._makeIcon();
		
		
        this._layers = {};
        this.dbp.style = document.createElement("style");
        this.dbp.style.innerHTML = ".dbpPopup>img {width:188px;};";
        document.body.appendChild(this.dbp.style);
    },
    onAdd: function (map) {
        this.dbp.map = map;
        this._map = map;
        this.dbp.layer = this;
        var _this = this;
		map.addLayer(markers);
		
		
		
		
		
        map.on("moveend",
            function () {
                if (map.hasLayer(_this)) {
                    var bounds = map.getBounds(),
                        SW = bounds._southWest,
                        NE = bounds._northEast,
						center = bounds.getCenter(),
                        areaToLoad = _this.dbp.utils._identifyAreaToLoad({SW: SW, NE: NE, center: center}, _this.dbp.visitedBounds);
                    //console.log("to_load: ", areaToLoad)
                    if (areaToLoad) {
                        _this.dbp._ajaxWrapper(areaToLoad.current, areaToLoad.not);
                    }
                    _this.dbp.visitedBounds.push({SW: SW, NE: NE, center: center});
                }
            }
        );
    },
	
	onRemove: function (map) {
        this.dbp.map = map;
        this._map = map;
        this.dbp.layer = this;
        var _this = this;
	map.removeLayer(markers);
	},
	
    dbp: {
        utils: {},
        queries: {},
        transport: {},
        visitedBounds: [],
        _ajaxWrapper: function (newArea, notHere) {
            var query = this.queries._assembleAreaQuery(newArea.SW, newArea.NE, newArea.center,
                                                               {notHere: notHere,
                                                                language: this.prefs.lang || "en",
                                                                includeCities: this.prefs.includeCities});
			//console.log(newArea.center);
            var url = this.queries._assemblepoliceDbURL(query);
            var _this = this;
            this._putLoader();
			
            this.transport._sendQuery(url, this._handlepoliceDbData,
                                {errorCallback:
                                    function () {
                                        notHere.pop();
                                    },
                                 completeCallback:
                                     function () {
                                            _this._removeLoader();
                                        }
                                });
        },

        _handlepoliceDbData: function (data) {
            var list = data;
			////console.log(data);
            L.policeLayer.prototype.dbp._addpoliceLayer(list);
        },

        _addpoliceLayer: function (list) {
            var idx;
			var markerStore = [];
			var asbicon = L.icon({
                iconUrl: "js/images/asb.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });

var arsonicon = L.icon({
                iconUrl: "js/images/arson.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });	
			

var bikeicon = L.icon({
                iconUrl: "js/images/bike.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });	


var vehicleicon = L.icon({
                iconUrl: "js/images/vehicle.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });
			
var violenceicon = L.icon({
                iconUrl: "js/images/violence.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });	

var drugsicon = L.icon({
                iconUrl: "js/images/drugs.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });					
			
var pubordericon = L.icon({
                iconUrl: "js/images/puborder.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });				
			
var weaponsicon = L.icon({
                iconUrl: "js/images/weapons.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });
			
var thefticon = L.icon({
                iconUrl: "js/images/theft.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });
			
var othefticon = L.icon({
                iconUrl: "js/images/othertheft.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });

var burglaryicon = L.icon({
                iconUrl: "js/images/burglary.png",
				shadowUrl: 'js/images/shadoww.png',
				shadowSize:   [52, 57],
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });

            for (idx = 0; idx < list.length ; idx++) {
				
                var entry = list[idx],
                    position =  [entry.location.latitude, entry.location.longitude];
					if(visited.indexOf(entry.id) == -1) {
						markerStore.push({position: position, 
						category: entry.category.toProperCase(),
						date: entry.month,
						outcome: entry.outcome_status
						});
						visited.push(entry.id);
					}
				
					

                //var _mark = L.marker(position, {icon: this.icon}).bindPopup(text);
				}
				for(j = 0; j < markerStore.length; j++) {
				if(markerStore[j].outcome != null){
					var outcomes = markerStore[j].outcome.category;
				} else {
					var outcomes = "No data";
				}
				text = "<div class='dbpPopup'>";
                text += "<h4 class='dbpPopupTitle'>" + "Crime Details:" + "</h4>";
				text += "<b>Crime Type: </b>" + markerStore[j].category + "<br/>";
				text += "<b>Outcome: </b>" + outcomes + "<br/>";
				text += "<b>Date: </b>" + markerStore[j].date + "<br/>";
				text += "<b>Location: </b>" + markerStore[j].position + "<br/>";
				
                text += "</div>"; 
				
				if(markerStore[j].category == "Anti-social-behaviour") {
					var crimeicon = asbicon;
				} else if (markerStore[j].category ==  "Bicycle-theft") {
					var crimeicon = bikeicon;
				} else if (markerStore[j].category == "Burglary"){
					var crimeicon = burglaryicon;
				} else if (markerStore[j].category == "Criminal-damage-arson") {
					var crimeicon = arsonicon;
				} else if (markerStore[j].category == "Drugs") {
					var crimeicon = drugsicon;
				} else if (markerStore[j].category == "Other-theft" || markerStore[j].category == "Robbery"){
					var crimeicon = thefticon;
				} else if (markerStore[j].category == "Theft-from-the-person") {
					var crimeicon = othefticon;
				} else if (markerStore[j].category == "Public-order") {
					var crimeicon = pubordericon;
				} else if (markerStore[j].category == "Vehicle-crime"){
					var crimeicon = vehicleicon;
				} else if (markerStore[j].category == "Violent-crime") {
					var crimeicon = violenceicon;
				} else {
					var crimeicon = this.icon;
				}
				
				var _mark = L.marker(markerStore[j].position, {icon: crimeicon}).bindPopup(text);
				
				
				
				if(this.map.getZoom() >= 13) {

                markers.addLayer(_mark);
				} else {
				
				}
				//console.log(_mark);
				}

            
        },
        _putLoader: function () {
            if (typeof this.loaderGif === "undefined") {
                var gif = document.createElement("img");
                gif.src = this.prefs.loaderGif || "data:image/gif;base64," + loaderB64Src;
                gif.id = "policeLayer.loaderGif";
                gif.style.position = "absolute";
                gif.style.width = "64px";
				
                gif.style.top = "14px";
                gif.style.left =  "48%";
                this.loaderGif = gif;
                var parentElement = this.map.getContainer();
                parentElement.appendChild(gif);
                //L.policeLayer.jMap.append(gif);
            } else {
                this.loaderGif.style.display = "block";
            }
        },
        _removeLoader: function () {
            this.loaderGif.style.display = "none";
        },
        _makeIcon: function () {
            return L.icon({
                iconUrl: "js/images/crime.png",
				shadowUrl: 'js/images/shadoww.png',
				 shadowSize:   [52, 57], 
                iconSize: (this.prefs.icon && this.prefs.icon.iconSize) || [45, 48],
                iconAnchor: (this.prefs.icon && this.prefs.icon.iconAnchor) || [16, 35],
                popupAnchor: (this.prefs.icon && this.prefs.icon.popupAnchor) || [-6, -25]
            });
        }

    }
});

L.policeALayer = function (options) {
    return new L.policeLayer(options || {});
};





/*global escape */
(function (exports) {
    function _assemblepoliceDbURL(query) {
        return "https://data.police.uk/api/crimes-street/all-crime?" + query;
    }
    exports._assemblepoliceDbURL = _assemblepoliceDbURL;
    exports._assembleAreaQuery = function (positionSW, positionNE, center, options) {
		point = String(center).replace("LatLng(","").replace(" ","").replace(")","").split(",");
        options = options || {};
         var q = "lat=" + point[0] + "&lng=" + point[1];
	   return q;
    };
})(typeof exports === "undefined" ?  L.policeLayer.prototype.dbp.queries : exports);


(function (exports) {
    exports._cleanupTypes = function (types) {
        types = types.replace(/,place|place,/, "");
        var split = types.split(",");
        split = split.sort(function (a, b) {
            return a.length - b.length;
        });
        return split.join(", ");
    };
    exports._langLink = function (url, lang) {
        if (lang === "en") {
            return url;
        }
        return url;
    };

    exports._shortenAbstract = function (abs) {
        var split = abs.split(" ");
        return split.slice(0, 24).join(" ") + (split.length > 24 ? "..." : "");
    };

    function _identifyAreaToLoad(current, priorAreas) {
        var not = [];
        for (var idx = 0 ; idx < priorAreas.length ; idx++) {
            var area = priorAreas[idx];
            if (_overlapping(current, area)) {
                not.push(area);
            }
        }
        return {"current": current, not: not};
    }
    exports._identifyAreaToLoad = _identifyAreaToLoad;

    function _overlapping(areaA, areaB) {
        if (offNorth(areaA, areaB) ||
            offEast(areaA, areaB) ||
            offSouth(areaA, areaB) ||
            offWest(areaA, areaB)) {
            return false;
        } else {
            return true;
        }
    }
    exports._overlapping = _overlapping;

    function offNorth(areaA, areaB) {
        // areaB lies north of areaA
        return northBound(areaA) < southBound(areaB);
    }

    function offEast(areaA, areaB) {
        // areaB lies east of areaA
        return eastBound(areaA) < westBound(areaB);
    }

    function offSouth(areaA, areaB) {
        // areaB lies east of areaA
        return southBound(areaA) > northBound(areaB);
    }

    function offWest(areaA, areaB) {
        // areaB lies west of areaA
        return westBound(areaA) > eastBound(areaB);
    }

    function northBound(area) {
        return area.NE.lat;
    }

    function eastBound(area) {
        return area.NE.lng;
    }

    function southBound(area) {
        return area.SW.lat;
    }

    function westBound(area) {
        return area.SW.lng;
    }
})(typeof exports === "undefined" ?  L.policeLayer.prototype.dbp.utils : exports);


/* jshint unused: false, maxlen: false */
var loaderB64Src = "R0lGODlhLQAtAPMPANTU1O3t7fJoRfv7++9OJdvb2+Lf3/SBZPixn/7v6/m+r/b29ubm5u0wAM3Nzf///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAPACwAAAAALQAtAAAE//DJSesIBjg3WxMHklRkaUpBsa2c5L0fcs5VoLFrB7+ETJsDFY6l270Eox8lMBwWjS+fktnEPaEehVJiqBJd2NdhOul6ARNCuDFGnZiG8tAQGFQSioOx/egGSgsrcVwrDHYzCXoefGYOCyRCG4N9AI9bBgSMLAU1c1s0jSt/Ezc4k58VoStoKFWsqBWlOKOROJawFIFNnANVDLglDFUXw8AkvU0YTafGcnOyos0kVDjQK4fSE8heLK/ZpE3f4uPk5RVN3uLWXuXb1cnk1N2qkuT0DnTF3+4sdb7iwprYqcUCmzF+Kzg9kNct2zoHox6sY4brnjeG+MTRiyih1qQMBltpDADwcRMJXRkJbTAkMmDKPituLXmpiiTHCcpMybm5xJkrcF4m8Sxxz4oEbvW2YAx3FCnET0uNPnA6dMYCglK5FZCJykaVCa6qdsUKFkcBscAuZNhQ1mbIGREAACH5BAUKAA8ALBgAAAAVABUAAARg0Lliwng46y37DFuIeR4AihlJFheqqmf4wuLsGShgOzimhIOAQdV7HBoI1IDRKR4bjQTqsQA4oVDBdPPEIreYrpcAfhC83t/WgMZqwWLvotyGJuH1Q1lRf28TdQ1lZnURACH5BAUKAA8ALCIABwALAB8AAARe8EkZppXG1fuyc8PlfYU1fhqGroAErGu1wGj5MPQXPnna5QZKzjboTV40jnLJ5BAa0GhDkpBKJQorVCA5aBuHR/WLeHi/Cca3wX1+FeYvYXKWlulS7qWeUHrvSnAWEQAh+QQFCgAPACwYABgAFQAVAAAEZ/DJSSdwOLvK39BaVwUgVoiUUToGKn1r4D7M6gzuYp/uFc+qEmAmCWpkHQPhMDE6eJXEoUFlFjO4SUIxpXqtDxVSQvCav5Ox5MxugCtttqITNyNE9YYggYoT7i5sAnNEVAIHCHxEEhEAIfkEBQoADwAsBwAiAB8ACwAABFrwSXmImTjPBa6mTXh82cA4qJcdYdscSlIGBmo7KujuxATcQNyEtStOgsGLosg8IoGBB4K5cz5RUUlCQA1ZkYWBBkGgfm+ALEnBrUqCBTVpkkAc2s6CISD+RAAAIfkEBQoADwAsAAAYABUAFQAABF+wydnIuzhjSpP+j8BJCqgdY3OYGZI2Hvsk7yqHr3Err3UTKZvMxRHeRBOhAbBg0SRKh5TBckWlWENg8CldDNgwFmACi8+gwHltUq/DrEHhLb0FAO/bJWCG6y8DfHMOEQAh+QQFCgAPACwAAAcACwAfAAAEYPBJqaaVqJ0rU/vbJXyglZGlRKDkprAk8YxwY3j1dsLSkUs0VuVRayQkNRlnyWxeHNCoAzCRSgeSghUakBi2DoMkAHZgB2VGtrx4kMHUBwAsdm/r6yhegobuJ2R/Fl0WEQAh+QQFCgAPACwAAAAAFQAVAAAEYvDJSWtCR7RWu1TaJnoUQogoRyZhOnqI63qKPHuHjVbBlOsESsBh8LkOigRl4GgWJb/GgVRoOn2EZ2dovZIogK5VS+KKHYCvpHp2LNTMNkP9MIvpD0ObTG336G0OA3htaXgRADs=";


/* jshint unused: false, maxlen: false */

(function (proto) {
    /**
     * sendQuery() sends an ajax call. it accepts the following params
     * @param {string} url
     * @param {function} successCallback
     * @param {object} options:
     *                     - trackLoading -> function, to visualize query progress
     *                     - errorCallback -> function
     *                     - completeCallback -> function
     * @returns {number} lastAjax
     */
    function sendQuery(url, successCallback, options) {
        options = options || {};
        var ajaxSetup = {
            dataType: "json",
            url: url,
            success: successCallback
        };
        if (options.completeCallback) {
            ajaxSetup.complete = function () {
                options.completeCallback();
            };
        }
        if (options.errorCallback) {
            ajaxSetup.error = options.errorCallback;
        }
        if (options.trackLoading) {
            ajaxSetup.progress = options.trackLoading;
        }
        var req = new XMLHttpRequest();
        req.open("GET", ajaxSetup.url, true);
        req.onreadystatechange = function () {
            if (req.readyState !== 4 || req.status !== 200) {
                return;
            } else {
                var data = (JSON.parse(req.responseText));
                ajaxSetup.success(data);
				////console.log(data);
            }
            ajaxSetup.complete();
        };
        req.send();
        proto.currentAjax = req;
        return req;
    }
    proto._sendQuery = sendQuery;
})(typeof exports === "undefined" ?  L.policeLayer.prototype.dbp.transport : exports);


