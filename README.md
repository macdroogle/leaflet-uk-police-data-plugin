# Leaflet UK Police Data Plugin

A Leaflet JS plugin which plots clustered marker points of UK street level crimes using the [UK Police API](https://data.police.uk/docs/).

## Dependencies

* [Leaflet Marker Cluster](https://github.com/Leaflet/Leaflet.markercluster)
* [LeafletJS] (http://leafletjs.com/) - _obviously_
